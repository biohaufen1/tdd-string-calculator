package tdd.string.calculator;

import java.util.Arrays;

public class StringCalculator {
    public int add(String numbers) {
        if (numbers.contains(",\n")) {
            throw new NumberFormatException("Input is not allowed!");
        }
        return  numbers.equals("") ? 0 : Arrays.stream(numbers.split("[,|\n]")).mapToInt(Integer::parseInt).sum();
    }
}
